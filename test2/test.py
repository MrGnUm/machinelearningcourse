import numpy as np
import pandas
from sklearn.tree import DecisionTreeClassifier


data = pandas.read_csv('titanic.csv')
data = data[['Age', 'Sex', 'Pclass', 'Fare', 'Survived']].dropna()
targetData = data['Survived']
filteredData = data[['Pclass', 'Fare', 'Age', 'Sex']]
filteredData.loc[:, 'Sex'] = filteredData['Sex'].replace(['male', 'female'], [0, 1])
print(filteredData)

# clf = DecisionTreeClassifier(random_state=241)
# clf.fit(filteredData, targetData)

X = np.array(filteredData)
y = np.array(targetData)

clf = DecisionTreeClassifier(random_state=241)
clf.fit(X, y)


importance = clf.feature_importances_
print(importance)
