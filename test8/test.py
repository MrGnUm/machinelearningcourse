import math
import pandas

# Загружаем данные
from sklearn.metrics import roc_auc_score

data = pandas.read_csv('data-logistic.csv', header=None)
y = data[0]
X = data.iloc[:, 1:]
# print(y)
# print(X)


# cоздаем функции для градиентного спуска
def fnw1(x, y, w1, w2, k, C):
    l = len(y)
    sum = 0
    for i in range(0, l):
        sum += y[i] * X[1][i] * (1 - (1 / (1 + math.exp(-y[i] * (w1 * X[1][i] + w2 * x[2][i])))))

    return w1 + ((k / l) * sum) - (k * C * w1)


def fnw2(x, y, w1, w2, k, C):
    l = len(y)
    sum = 0
    for i in range(0, l):
        sum += y[i] * X[2][i] * (1 - (1 / (1 + math.exp(-y[i] * (w1 * X[1][i] + w2 * x[2][i])))))

    return w2 + ((k / l) * sum) - (k * C * w2)


# функция реализации градиентного спуска
def grad(x, y, w1=0, w2=0, k=0.1, C=0, error=1e-5):
    i = 0
    max_i = 10000
    # w1New = w1
    # w2New = w2

    # // TODO возможно изменить услвония
    while True:
        i += 1
        w1New = fnw1(x, y, w1, w2, k, C)
        w2New = fnw2(x, y, w1, w2, k, C)
        err = math.sqrt(math.pow(w1New - w1, 2) + math.pow(w2New - w2, 2))

        if i >= max_i or err <= error:
            break
        else:
            w1 = w1New
            w2 = w2New

    return [w1New, w2New]


# запускаем всё это чудо
W = grad(X, y)
WL2 = grad(X, y, C=10)


#  определяем сигмоидную фукнцию a(x) = 1 / (1 + exp(-w1 x1 - w2 x2)).
def a(x, w1, w2):
    return 1 / (1 + math.exp(-w1 * x[1] - w2 * x[2]))


w1, w2 = W
wl2_1, wl2_2 = WL2

score = X.apply(lambda x: a(x, w1, w2), axis=1)
scoreL2 = X.apply(lambda x: a(x, wl2_1, wl2_2), axis=1)

aucRoc = roc_auc_score(y, score)
aucTocL2 = roc_auc_score(y, scoreL2)

print("{:0.3f} {:0.3f}".format(aucRoc, aucTocL2))