import pandas
from sklearn.svm import SVC

data = pandas.read_csv('svm-data.csv', header=None)
targetData = data[0]
# print(targetData)
# attrData = data[[1, 2]]
attrData = data.iloc[:, 1:]
# print(attrData)

svc = SVC(C=100000, kernel='linear', random_state=241)
svc.fit(attrData, targetData)
print(svc.support_)
