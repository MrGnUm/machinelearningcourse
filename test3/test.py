import pandas
import numpy as np
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale


data = pandas.read_csv('wine.data')
# print(data)
# print('----------------------------------------------------------------')

cls = data.iloc[:, 0]
# print(cls)

attributes = data.drop(data.columns[0], axis=1)
# print(attributes)
# attributes = scale(attributes)
# print(attributes)


#  До скалирования
kf = KFold(n_splits=5, shuffle=True, random_state=42)
print(kf)

k = 1
optimumK = k
optimumResult = 0
while k <= 50:
    knn = KNeighborsClassifier(n_neighbors=k)
    result = cross_val_score(knn, attributes, cls,  cv=kf)
    resMean = np.mean(result)
    print(k, resMean)
    if (resMean > optimumResult):
        optimumResult = resMean
        optimumK = k
    k += 1

print("OptimumK = ", optimumK, "Value: ", round(optimumResult, 2))



# после скалировани

# TODO вынести в функцию все это

attributes = scale(attributes)
k = 1
optimumK = k
optimumResult = 0
while k <= 50:
    knn = KNeighborsClassifier(n_neighbors=k)
    result = cross_val_score(knn, attributes, cls,  cv=kf)
    resMean = np.mean(result)
    print(k, resMean)
    if (resMean > optimumResult):
        optimumResult = resMean
        optimumK = k
    k += 1

print("OptimumK = ", optimumK, "Value: ", round(optimumResult, 2))



