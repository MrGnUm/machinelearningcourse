import math
import pandas
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.metrics import log_loss
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

data = pandas.read_csv('gbm-data.csv')

y = data['Activity'].values
X = data.loc[:, 'D1':'D1776'].values

# Получаем тестовую и обучающие выборки
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.8, random_state=241)


# Преобразуйте полученное предсказание с помощью сигмоидной функции по формуле 1 / (1 + e^{−y_pred})
def sig_fun(y_pred):
    return 1 / (1 + math.exp(-y_pred))


# Используйте метод staged_decision_function для предсказания качества на обучающей и тестовой выборке
def log_loss_res(clf, y, X):
    results = []
    for pred in clf.staged_decision_function(X):
        # Преобразуем занчения
        y_ar_pred = []
        for y_pred in pred:
            y_ar_pred.append(sig_fun(y_pred))

        results.append(log_loss(y, y_ar_pred))

    return results


# Строим график
def create_chart(test_loss, train_loss, i):
    plt.figure()
    plt.plot(test_loss, 'r', linewidth=2)
    plt.plot(train_loss, 'g', linewidth=2)
    plt.legend(['test', 'train'])
    plt.show()
    plt.savefig('chart_' + str(i) + '.png')


# Перебираем по заданным rate
min_loss = {}
for learning_rate in [1, 0.5, 0.3, 0.2, 0.1]:
    clf = GradientBoostingClassifier(n_estimators=250, verbose=True, random_state=241, learning_rate=learning_rate)
    clf.fit(X_train, y_train)

    train_loss = log_loss_res(clf, y_train, X_train)
    test_loss = log_loss_res(clf, y_test, X_test)
    create_chart(test_loss, train_loss, learning_rate)
    #  ищем минимальное значение метрик
    # print(test_loss)
    min_loss_value = min(test_loss)
    min_loss_index = test_loss.index(min_loss_value)
    min_loss[learning_rate] = [min_loss_value, min_loss_index]

print('\n', min_loss)

min_loss_v, min_loss_i = min_loss[0.2]
print('\n', 'min_loss at rate = 0.2: value={:0.2f}, index={}'.format(min_loss_v, min_loss_i))

# Ищем значение log-loss у случайного леса
rf_cls = RandomForestClassifier(n_estimators=min_loss_i, random_state=241)
rf_cls.fit(X_train, y_train)
predict = rf_cls.predict_proba(X_test)
loss = log_loss(y_test, predict[:, 1])
print('loss at RandomForestClassifier: {:0.2f}'.format(loss))






