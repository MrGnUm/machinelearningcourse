import pandas
import numpy as np
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler


#  Получаем тренировочные данные
trainData = pandas.read_csv('perceptron-train.csv', header=None)
train = {'target': trainData[0], 'attr': trainData.iloc[:, 1:]}
# print(train)
# print('----------------------------')

# Получаем тестируемые данные
testData = pandas.read_csv('perceptron-test.csv', header=None)
test = {'target': testData[0], 'attr': testData.iloc[:, 1:]}
# print(test)

# обучаем персептрон
model = Perceptron(random_state=241)
model.fit(train['attr'], train['target'])
predict = model.predict(test['attr'])

accuracyBeforeScale = accuracy_score(test['target'], predict)
print('accuracy before scale:', accuracyBeforeScale)

# нормализуем выборки
scaler = StandardScaler()
scalerTrain = scaler.fit_transform(train['attr'])
scalerTest = scaler.transform(test['attr'])


# обучаем персептрон на нормализованной выборке
model = Perceptron(random_state=241)
model.fit(scalerTrain, train['target'])
predict = model.predict(scalerTest)

accuracyAfterScale = accuracy_score(test['target'], predict)
print('accuracy after scale:', accuracyAfterScale)

print ('diff:' , accuracyAfterScale - accuracyBeforeScale)