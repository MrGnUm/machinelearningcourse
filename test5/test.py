import pandas
import numpy as np
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

trainData = pandas.read_csv('perceptron-train.csv', header=None)
train = {'target': trainData.iloc[:, 0], 'attr': trainData.iloc[:, [1, 2]]}
# print(train)
# print('----------------------------')

testData = pandas.read_csv('perceptron-test.csv', header=None)
test = {'target': testData.iloc[:, 0], 'attr': testData.iloc[:, [1, 2]]}
# print(test)


#  До номрализации обучающей и тестовой выборки
clf = Perceptron(random_state=241)
clf.fit(train['attr'], train['target'])
predictions = clf.predict(test['attr'])
accuracyBeforeScale = accuracy_score(test['target'], predictions)
print('accuracy before scale:', accuracyBeforeScale)





# Нормализуем данные
scaler = StandardScaler()
scaledTrainData = pandas.DataFrame(scaler.fit_transform(trainData))
scaledTrain = {'target': trainData.iloc[:, 0], 'attr': scaledTrainData.iloc[:, [1, 2]]}
# print(scaledTrain)

scaledTestData = pandas.DataFrame(scaler.transform(testData))
scaledTest = {'target': testData.iloc[:, 0], 'attr': scaledTestData.iloc[:, [1, 2]]}



#  Запускает еще раз, но уже на скалированных данных
# TODO Попробовать написать фукнцию
clf = Perceptron(random_state=241)
clf.fit(scaledTrain['attr'], scaledTrain['target'])
predictions = clf.predict(scaledTest['attr'])
accuracyAfterScale = accuracy_score(scaledTest['target'], predictions)
print('accuracy after scale:', accuracyAfterScale)


print('diff', abs(round(accuracyBeforeScale - accuracyAfterScale, 3)))