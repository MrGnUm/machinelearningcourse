import pandas
import math
from skimage import img_as_float
from skimage.io import imread
import pylab
import numpy as np
from sklearn.cluster import KMeans

image = img_as_float(imread('parrots.jpg'))
size = {}
size['width'], size['height'], size['form'] = image.shape
print(size)
# преобразуем в двухмерную матрицу, где количество строк - это количесво всех пикселей (width*height)
# а количество столбцов - соответствует цветам RGB
reshaped_image = np.reshape(image, (size['width']*size['height'], size['form']))
pixels = pandas.DataFrame(reshaped_image, columns=['R', 'G', 'B'])
# print(pixels)


def k_means(pixels, n):
    px = pixels.copy()
    #  init='k-means++' - по умолчанию, но раз указано его использовать - передадим =)
    clf_k_means = KMeans(random_state=241, n_clusters=n, init='k-means++')
    px['clusters'] = clf_k_means.fit_predict(pixels)
    # print(px)
    mean_img = make_mean_image(px, n, type='mean')
    median_img = make_mean_image(px, n, type='median')
    return [mean_img, median_img]


# Создаем картинку либо по медиане, либо по среднему, в зависимости от переданного типа
def make_mean_image(px, n, type='mean'):
    # medorme - median or means, не смог придумать имя переменной =)
    if type == 'mean':
        medorme = px.groupby('clusters').mean().values
    else:
        medorme = px.groupby('clusters').median().values

    medorme_px = []
    for i in px['clusters'].values:
        medorme_px.append(medorme[i])
    image = np.reshape(medorme_px, (size['width'], size['height'], size['form']))
    pylab.imsave(type + '/parrots_n' + str(n) + '.jpg', image)

    return image

# вычисляем метрику PSNR
def psnr(image1, image2):
    mse = np.mean((image1 - image2) ** 2)
    return 10 * math.log10(float(1) / mse)


for i in range(1, 25):
    mean_img, median_img = k_means(pixels, i)
    mean_psnr = psnr(image, mean_img)
    median_psnr = psnr(image,median_img)
    print("i={}, mean_psnr={:0.2f}, median_psnr={:0.2f}".format(str(i), mean_psnr, median_psnr))
    if median_psnr > 20 or mean_psnr > 20:
        print('Bingo!')


