import numpy as np
import pandas
import re

# X = np.random.normal(loc=1, scale=10, size=(1000, 50))
# print(X)
# print('---------------------')
#
# m = np.mean(X, axis=0)
# std = np.std(X, axis=0)
# X_norm = ((X - m) / std)
# print(X_norm)
# print('---------------------')
#
# Z = np.array([[4, 5, 0],
#               [1, 9, 3],
#               [5, 1, 1],
#               [3, 3, 3],
#               [9, 9, 9],
#               [4, 7, 1]])
#
# r = np.sum(Z, axis=1)
# print(np.nonzero(r > 10))
# print('---------------------')
#
# A = np.eye(3)
# B = np.eye(3)
# print(A)
# print('\n')
# print(B)
# print('\n')
#
# C = np.hstack((A, B))
# print(C)
# print('\n')


data = pandas.read_csv('titanic.csv', index_col='PassengerId')
print('1) male/female on the board\n')
print(data['Sex'].value_counts())
print('\n')

survived = data['Survived'].value_counts()
print('2) Survived: ', round(survived[1] / data['Survived'].count(),4) * 100, '%')
print('\n')

ClassPass = data['Pclass'].value_counts()
print(ClassPass)
print('3) first Class: ', round(ClassPass[1] / data['Pclass'].count(), 4) * 100, '%')
print('\n')

ages = data['Age'].value_counts()
print('4) Ages. mean: ', round(data.mean()['Age'], 2), '; median:', round(data.median()['Age'], 2))
print('\n')

print('5) Correlation SibSp / Parch:', data['SibSp'].corr(data['Parch']))
print('\n')


allNames = data['Name'].values
firstNames = {}
maxNameCount = 0
maxName = '';
for name in allNames:
    m = re.search('Ms.|Miss.[\s]+([\w\s]+)', name)
    if m is not None:
        if m.group(1) in firstNames:
            firstNames[m.group(1)] += 1
        else:
            firstNames[m.group(1)] = 1

        if int(firstNames[m.group(1)]) > maxNameCount:
            maxNameCount = int(firstNames[m.group(1)])
            maxName = m.group(1)

# print(firstNames)
print("6) Max name", maxName, maxNameCount)
# print(maxName)
