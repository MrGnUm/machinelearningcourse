import pandas
from scipy.sparse import hstack
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge

data_train = pandas.read_csv('salary-train.csv')

# Заменим все, кроме букв и цифр, на пробелы
data_train['FullDescription'] = data_train['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True)

# Приводим к нижнему регистру
data_train['FullDescription'] = data_train['FullDescription'].map(lambda txt: txt.lower())
# print(data_train['FullDescription'])

# Оставим только те слова, которые встречаются хотя бы в 5 объектах
vec = TfidfVectorizer(min_df=5)
X_data_train = vec.fit_transform(data_train['FullDescription'])
# print(X_data_train)

#  Заполнянет строкой 'nan' пропуски
data_train['LocationNormalized'].fillna('nan', inplace=True)
data_train['ContractTime'].fillna('nan', inplace=True)

# Применяем DictVectorizer для получения one-hot-кодирования признаков LocationNormalized и ContractTime.
dic_vec = DictVectorizer()
X_train_categ = dic_vec.fit_transform(data_train[['LocationNormalized', 'ContractTime']].to_dict('records'))


X_train = hstack([X_data_train, X_train_categ])
# print(X_train)

# Обучаем гребневую регрессию
ridge = Ridge(alpha=1, random_state=241)
ridge.fit(X_train, data_train['SalaryNormalized'])

# TODO возможно стоит реализовать отдельную функцию для трансформации текста в нижний регистр + замена всех лишних символов на пробелы

data_test = pandas.read_csv('salary-test-mini.csv')
# Заменим все, кроме букв и цифр, на пробелы
data_test['FullDescription'] = data_test['FullDescription'].replace('[^a-zA-Z0-9]', ' ', regex=True)
# Приводим к нижнему регистру
data_test['FullDescription'] = data_test['FullDescription'].map(lambda txt: txt.lower())
X_data_test = vec.transform(data_test['FullDescription'])
X_test_categ = dic_vec.transform(data_test[['LocationNormalized', 'ContractTime']].to_dict('records'))
X_test = hstack([X_data_test, X_test_categ])

result = ridge.predict(X_test)
print('answer: {:0.2f} {:0.2f}'.format(result[0], result[1]))




