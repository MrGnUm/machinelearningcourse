from sklearn.datasets import load_boston
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsRegressor
from sklearn.preprocessing import scale
import numpy as np


def debug(_data):
    print(_data)


data = load_boston()
data['data'] = scale(data['data'])
# print(data['data'])
# print(data['target'])

parameters = np.linspace(1, 10, num=200)
# debug(parameters)

kf = KFold(n_splits=5, shuffle=True, random_state=42)
debug(kf)

optimumP = 0
maximumRes = -9999999999999999999999
for i in parameters:
    neighbors = KNeighborsRegressor(n_neighbors=5, weights='distance', p=i)

    result = cross_val_score(
                    neighbors,
                    data['data'],
                    data['target'],
                    scoring='neg_mean_squared_error',
                    cv=kf
            )
    mean = result.mean()
    print('i =', i,  mean)
    if (mean > maximumRes):
        maximumRes = mean
        optimumP = i


print('p:', round(optimumP, 1), 'mean = ', maximumRes)




