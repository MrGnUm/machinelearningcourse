import pandas
from numpy import corrcoef
from sklearn.decomposition import PCA

data = pandas.read_csv('close_prices.csv')
# all data without date
X = data.loc[:, 'AXP':]

pca = PCA(n_components=10)
pca.fit(X.values)

component_number = 0
dispersion = 0

for disp in pca.explained_variance_ratio_:
    component_number += 1
    dispersion += disp
    if dispersion >= 0.9:
        break

print("components_number = {}, dispersion = {:0.2f}".format(str(component_number), dispersion))

data_comp = pandas.DataFrame(pca.transform(X))
data_comp0 = data_comp[0]

jon_data = pandas.read_csv('djia_index.csv')
djia = jon_data['^DJI']

corr = corrcoef(data_comp0, djia)
# print('correlation = {:0.2f}'.format(corr))
print(corr)


top_company_number = pandas.Series(pca.components_[0]).sort_values(ascending=False).head(1).index[0]
top_company = X.columns[top_company_number]
print(top_company)


