import numpy
import pandas
from sklearn.model_selection import KFold, cross_val_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score

data = pandas.read_csv('abalone.csv')
# transform "Sex" field M -> 1, F -> -1, I -> 0
data['Sex'] = data['Sex'].map(lambda x: 1 if x == 'M' else (-1 if x == 'F' else 0))
# print(data)


X = data.loc[:, :'ShellWeight']
y = data['Rings']
# print(X)
# print(Y)


kf = KFold(random_state=1, shuffle=True, n_splits=5)

scores = []
for n in range(1, 51):
    RFR = RandomForestRegressor(random_state=1, n_estimators=n)
    RFR.fit(X, y)
    # predict_y = RFR.predict(X)
    # score = r2_score(y, predict_y)
    score = numpy.mean(cross_val_score(RFR, X, y, cv=kf, scoring='r2'))
    print(n, score)
    scores.append(score)

for n in range(0, len(scores)):
    print(n)
    if scores[n] > 0.52:
        print('answer, tree numbers: {}'.format(str(n+1)))
        break
