import pandas
from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.svm import SVC

# Получаем данные
newsgroups = datasets.fetch_20newsgroups(
                    subset='all',
                    categories=['alt.atheism', 'sci.space']
             )

X = newsgroups.data
y = newsgroups.target

#  Обучаем
tfid = TfidfVectorizer()
tfid.fit_transform(X)

grid = {'C': np.power(10.0, np.arange(-5, 6))}
kf = KFold(n_splits=5, random_state=241, shuffle=True)
svc = SVC(kernel='linear', random_state=241)
gs = GridSearchCV(svc, grid, scoring='accuracy', cv=kf)
gs.fit(tfid.transform(X), y)


score = 0
C = 0
for sc in gs.grid_scores_:
    if sc.mean_validation_score > score:
        score = sc.mean_validation_score
        C = sc.parameters['C']

print("score = ", score, 'C=', C)

svc = SVC(kernel='linear', random_state=241, C=C)
svc.fit(tfid.transform(X), y)
words = tfid.get_feature_names()
# print(svc.coef_.data)
coef = pandas.DataFrame(svc.coef_.data, svc.coef_.indices)
top_words = coef[0].map(lambda w: abs(w)).sort_values(ascending=False).head(10).index.map(lambda i: words[i])
top_words.sort()
print(1, ','.join(top_words))
