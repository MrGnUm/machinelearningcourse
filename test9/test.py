import pandas
import sklearn.metrics as metrics

data = pandas.read_csv('classification.csv')
# print(data)

classification = {'tp': 0, 'fp': 0, 'fn': 0, 'tn': 0}

# Перебираем все эелементы
for i in range(0, len(data)):
    #  возможно это можно сделать как то без обилия if  =)
    #  находим True Positive
    if (data['true'][i] == 1 and data['pred'][i] == 1):
        classification['tp'] += 1
    # Находим True Negative
    if (data['true'][i] == 0 and data['pred'][i] == 0):
        classification['tn'] += 1
    #  Находим False Positive
    if (data['true'][i] == 0 and data['pred'][i] == 1):
        classification['fp'] += 1
    # Находим False Negative
    if (data['true'][i] == 1 and data['pred'][i] == 0):
        classification['fn'] += 1

print('1)', classification, '\n')

# Доля верноугаданных ответов
accuracy = round(metrics.accuracy_score(data['true'], data['pred']), 2)
# Точность
precision = round(metrics.precision_score(data['true'], data['pred']), 2)
# Полнота
recall = round(metrics.recall_score(data['true'], data['pred']), 2)
# F-мера
fmera = round(metrics.f1_score(data['true'], data['pred']), 2)
print('2)','{:0.2f} {:0.2f} {:0.2f} {:0.2f}\n'.format(accuracy, precision, recall, fmera))

# ---------------------------------------------------------------------------------------------------------------------

data = pandas.read_csv('scores.csv')
# print(data)
scores = {}
# Считаем площадь под кривой
for column in data.columns[1:]:
    scores[column] = metrics.roc_auc_score(data['true'], data[column])

print(scores)
max_scores = sorted(scores.items(), key=lambda item: item[1], reverse=True)[0]
print("3)", "max scores name: {}, values: {:0.2f}\n".format(max_scores[0], max_scores[1]))


#  Считаем нибольшую тчоность при полноте заданой больше 0.7
prc = {}
for column in data.columns[1:]:
    prc_tmp = metrics.precision_recall_curve(data['true'], data[column])
    prc_df = pandas.DataFrame({'precision': prc_tmp[0], 'recall': prc_tmp[1]})
    prc[column] = prc_df[prc_df['recall'] >= 0.7]['precision'].max()
    # print(prc_df[prc_df['recall'] >= 0.7]['precision'].max())

print(prc)
max_classificator = sorted(prc.items(), key=lambda item: item[1], reverse=True)[0]
# print(max_classificator)
print("4)", "max scores name: {}, values: {:0.2f}\n".format(max_classificator[0], max_classificator[1]))






